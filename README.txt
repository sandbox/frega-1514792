dnode_fpd
=========

Inspired by beejeebus' fdp module (http://drupal.org/project/fdp), we whipped a
similar small module into shape. It operates somewhat differently creating a
per request nonce/token.

In order to test this module, please do the following.

1) Download the dnode.module

cd sites/all/modules/
git clone --branch master frega@git.drupal.org:sandbox/frega/1321342.git dnode

2) Enable dnode and dnode_http (or dnode_php).

drush en -y dnode dnode_http

If you use dnode_php you need to run "composer" in the appropriate folder.
See the INSTALL.txt in sites/all/modules/dnode/modules/dnode_php/ for a quick
installation guide.

3) Enable dnode_fpd

drush en -y dnode_fpd

4) Start the dnode- and http-servers

drush dnode-nodejs-servers

5) In a new terminal call

drush dnode-fpd-url {FILE-ID} --uri="http://localhost"

Where --uri is the base-url of the installation

Additional options:

  --ip-address (or --ip, ip_address) - limit to delivery to this IP-Address
  --user-agent (or --user_agent) - limit to this User-Agent String
  --ttl - time-to-live for the download url in seconds.
  --tries - how many times can this link be "downloaded"
