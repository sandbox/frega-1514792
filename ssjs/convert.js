var fs = require('fs'),
  http = require('http'),
  spawn = require('child_process').spawn;
var reqCount = 0;
http.createServer(function(req, res) {
  if (req.url == '/favicon.ico') { return res.end(); }
  console.log('Incoming request %s', req.url);
  reqCount++;
  var filePath = __dirname + '/test.png';
  var now = reqCount + ':' + new Date().toString();
  var args = [filePath,
    '-resize', '600x600',
    '-pointsize', 30, '-fill', 'white',
    '-draw', 'text 0,100 "' + now + '"', '-'];
  var convert = spawn('convert', args);
  convert.stdout.pipe(res);
}).listen(2000);
console.log('Started a "watermarking" imagemagick server on :2000');
