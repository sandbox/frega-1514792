var spawn = require('child_process').spawn;
// DrupalDnode - provide by dnode.module (replace w/ seaport)
DrupalDnode = global.DrupalDnode;
var config = DrupalDnode.getModuleConfig('dnode_fpd');
var debug = config.debug || false;

// Primitive token store.
var tokens = {};
// Retrieve and validate token.
function validToken(token, ip) {
  if (typeof tokens[token] === 'undefined') {
    return false;
  }
  // Check expiry, downloadCount, tries
  var now = Math.floor(+(new Date()) / 1000);
  // Too verbose.
  // debug && console.log('Check token validity (' + now + ')', tokens[token]);
  if (
       (tokens[token].expiry < now)
    || (tokens[token]._downloadCount >= tokens[token].tries)
    || (ip && ip != tokens[token].ip)
  ) {
    debug && console.log('Token invalidated:', token);
    delete tokens[token];
    return false;
  }
  return tokens[token];
}
// Expressjs server instance.
var app = require('express').createServer();
app.configure(function(){
 app.use(app.router);
});
app.get('/download/:token', function (req, res) {
  var token = req.params.token || '';
  var tokenData = validToken(token);
  console.log('token', token, tokenData);
  if (tokenData) {
    if (tokenData.user_agent) {
      if (tokenData.user_agent != req.headers['user-agent']) {
        debug && console.log('403 - token user-agent differ', tokenData.user_agent, req.headers['user-agent']);
        return res.send(403);
      }
    }
    if (tokenData.ip_address) {
      var ip = req.socket.remoteAddress;
      // @todo: check headers ['x-forwarded-for', 'forwarded-for'] and
      // for remoteAdresses forwarding is accepted
      if (ip != tokenData.ip_address) {
        debug && console.log('403 - ip address differ', tokenData.ip_address, ip);
        return res.send(403);
      }
    }
    debug && console.log('Token - ', token, ' data', tokenData);
    if (tokenData.watermark) {
      var now = new Date().toString();
      var args = [tokenData.realpath, '-pointsize', 100, '-fill', 'white',
        '-draw', 'text 0,100 "' + now + '"', '-'];
      var convert = spawn('convert', args);
      res.attachment(tokenData.filename);
      convert.stdout.pipe(res)
      convert.stderr.pipe(process.stderr);
      return ;
    }
    // Pass on headers for original otherwise.
    for (var header in tokenData.headers) {
      res.setHeader(header, tokenData.headers[header]);
    }
    // Check for tries ...
    if (tokenData.tries == 1) {
      debug && console.log('Only one try, remove token immediately');
      delete tokens[token];
    } else {
      // Initialize if needed.
      if (typeof tokens[token]._downloadCount === 'undefined') {
        tokens[token]._downloadCount = 1;
      } else {
        tokens[token]._downloadCount++;
      }
      debug && console.log('Incremented _downloadCount for ' + token, ' current count: ', tokens[token]._downloadCount, ' of max ', tokenData.tries );
    }
    // this is just a expressjs wrapper around a fs.createReadStream().pipe().
    res.download(tokenData.realpath, tokenData.filename);
  } else {
    debug && console.log('No tokendata could be retrieved for token ', token);
    res.send(404);
  }
});
app.listen(config.http_port, config.http_host);

// Setup the the dnode server.
exports.setDownloadFileToken = function(token, data, cb) {
  console.log("setDownloadFileToken", token, data);
  tokens[token] = data;
  cb(null, true);
}
// Simple GC for tokens.
setInterval(function() {
  for (var k in tokens) {
    // Invalid tokens will be removed.
    validToken(k);
  }
}, 1000);