var fs = require('fs'), http = require('http');
http.createServer(function(req, res) {
  if (req.url == '/favicon.ico') { return res.end(); }
  // this is a big image ...
  var filePath = __dirname + '/test.png';
  var readStream = fs.createReadStream(filePath);
  readStream.pipe(res);
}).listen(2000);
console.log('Started a test.png-server listening on :2000');

