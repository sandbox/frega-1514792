<?php
/**
 * @file
 * dnode_fpd drush command.
 */

/**
 * Implements of hook_drush_command().
 */
function dnode_fpd_drush_command() {
  // Test strategies.
  $items['dnode-fpd-url'] = array(
    'callback' => 'dnode_fpd_generate_url',
    'description' => 'Get url for fast drupal file delivery - provide a fid!',
  );
  return $items;
}

/**
 * Generate urls from CLI.
 *
 * @param string $fid
 *   Managed file id.
 *
 * @return mixed
 *   URL is printed on stdout.
 */
function dnode_fpd_generate_url($fid) {
  if (!$fid) {
    return drush_print(t('Please provide a managed file id.'));
  }
  $options = array();
  $options['ip_address'] = drush_get_option(
    array('ip_address', 'ip-address', 'ip'), FALSE
  );
  $options['tries'] = drush_get_option('tries', NULL);
  $options['ttl'] = drush_get_option('ttl', NULL);
  $options['user_agent'] = drush_get_option(
    array('user-agent', 'user_agent'), NULL
  );
  $info = dnode_fpd_get_redirect_info($fid, $options);
  if ($info) {
    dnode_rpc('dnode_fpd', 'setDownloadFileToken', array($info['token'], $info['data']), function($err) use ($info) {
      if ($err) {
        drush_print(t('Could not set download file token.'), 'error');
        return FALSE;
      }
      drush_print(
        t('Open this URL to @url (in the next @ttl seconds)',
          array(
            '@url' => $info['url'],
            '@ttl' => isset($options['ttl']) ? $options['ttl'] : variable_get('dnode_fpd_token_ttl', '10'),
          )
        )
      );
    });
  }
  else {
    drush_print(
      t('File info could not be retrieved for file with id @fid - check permissions and/or make sure it exists!',
        array('@fid' => $fid)
      )
    );
  }
}
